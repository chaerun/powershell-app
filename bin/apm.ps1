#requires -v 3
param($cmd)

Set-StrictMode -Off

. "$PSScriptRoot\..\lib\core.ps1"
. (relpath '..\lib\commands')

$commands = commands
if (@($null, '--help', '/?') -contains $cmd -or $args[0] -contains '-h') { 
    exec 'help' $args 
} elseif ($commands -contains $cmd) { 
    exec $cmd $args 
} else { 
    "apm: '$cmd' isn't a apm command. See 'apm help'."
    exit 1
}
