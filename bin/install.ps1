# requires -v 3

# remote install:
#   iex (New-Object System.Net.WebClient).DownloadString('https://gitlab.com/chaerun/powershell-app/raw/master/bin/install.ps1')

# quit if anything goes wrong
$old_ErrorActionPreference = $ErrorActionPreference
$ErrorActionPreference = 'Stop' 

if (($PSVersionTable.PSVersion.Major) -lt 3) {
    Write-Output "PowerShell 3 or greater is required to run APM-DATA-Tools."
    Write-Output "Upgrade PowerShell: https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-windows-powershell"
    break
}

# show notification to change execution policy:
if ((get-executionpolicy) -gt 'RemoteSigned') {
    Write-Output "PowerShell requires an execution policy of 'RemoteSigned' to run APM-DATA-Tools."
    Write-Output "To make this change please run:"
    Write-Output "'Set-ExecutionPolicy RemoteSigned -scope CurrentUser'"
    break
}

# get core functions
$core_url = 'https://gitlab.com/chaerun/powershell-app/raw/master/lib/core.ps1'
Write-Output 'Initializing...'
Invoke-Expression (New-Object System.Net.WebClient).DownloadString($core_url)

# prep
if (installed) {
    Write-Host "APM-DATA-Tools is already installed. Run 'apm update' to get the latest version." -f red
    # don't abort if invoked with iex that would close the PS session
    if ($myinvocation.mycommand.commandtype -eq 'Script') { 
        return 
    } else { 
        exit 1 
    }
}
$dir = ensure($apmdir)

# download APM-DATA-Tools zip
$zipurl = 'https://gitlab.com/chaerun/powershell-app/-/archive/master/APM-DATA-Tools-master.zip'
$zipfile = "$dir\apm-tools.zip"
Write-Output 'Downloading...'
dl $zipurl $zipfile

'Extracting...'
unzip $zipfile "$dir\_tmp"
Copy-Item "$dir\_tmp\APM-DATA-Tools-master\*" $dir -r -force
Remove-Item "$dir\_tmp" -r -force
Remove-Item $zipfile

Write-Output 'Creating shim...'
shim "$dir\bin\apm.ps1"

ensure_path
success 'APM-DATA-Tools was installed successfully!'
Write-Output "Type 'apm help' for instructions."

# Reset $ErrorActionPreference to original value
$ErrorActionPreference = $old_ErrorActionPreference 