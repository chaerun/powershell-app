# Usage: apm uninstall
# Summary: Remove APM-DATA-Tools

. "$PSScriptRoot\..\lib\core.ps1"

warn 'This will uninstall APM-DATA-Tools!'
$answer = Read-Host 'Are you sure? (y/N)'
if ($answer -notlike 'y*') { 
    exit
}

rm_dir $apmdir
remove_path
success 'APM-DATA-Tools has been uninstalled.'

exit 0
