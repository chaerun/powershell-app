# Usage: apm update
# Summary: Update APM-DATA-Tools to the latest version

. "$PSScriptRoot\..\lib\core.ps1"

# download APM-DATA-Tools zip
$zipurl = 'http://usatl-s-elap01.ventyx.abb.com/idchmuh/APM-DATA-Tools/-/archive/master/APM-DATA-Tools-master.zip'
$zipfile = "$dir\apm-tools.zip"
Write-Output 'Downloading...'
dl $zipurl $zipfile

'Updating...'
unzip $zipfile "$dir\_tmp"
Copy-Item "$dir\_tmp\APM-DATA-Tools-master\*" $dir -r -force
Remove-Item "$dir\_tmp" -r -force
Remove-Item $zipfile

success 'APM-DATA-Tools was updated successfully!'

exit 0
