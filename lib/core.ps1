$apmdir = $env:APM_TOOLS, "$env:USERPROFILE\apm-tools" | Select-Object -first 1
$shimdir = "$apmdir\shims"

# messages
function abort($msg, [int] $exit_code=1) { Write-Host $msg -f red; exit $exit_code }
function error($msg) { Write-Host "ERROR $msg" -f darkred }
function warn($msg) { Write-Host "WARN  $msg" -f darkyellow }
function info($msg) { Write-Host "INFO  $msg" -f darkgray }
function debug($msg, $indent = $false) {
    if ($indent) {
        Write-Host "    DEBUG $msg" -f darkcyan
    } else {
        Write-Host "DEBUG $msg" -f darkcyan
    }
}
function success($msg) { write-host $msg -f darkgreen }

# apps
function installed() {
    return (Test-Path $apmdir) -and (Get-Item $apmdir) -is [System.IO.DirectoryInfo]
}

# paths
function fname($path) { Split-Path $path -leaf }
function strip_ext($fname) { $fname -replace '\.[^\.]*$', '' }
function strip_filename($path) { $path -replace [regex]::escape((fname $path)) }
function strip_fragment($url) { $url -replace (New-Object uri $url).fragment }

function ensure($dir) { 
    if (!(Test-Path $dir)) { 
        mkdir $dir > $null 
    }
    
    Resolve-Path $dir 
}

function fullpath($path) { # should be ~ rooted
    $executionContext.sessionState.path.getUnresolvedProviderPathFromPSPath($path)
}
function relpath($path) { "$($myinvocation.psscriptroot)\$path" } # relative to calling script
function friendly_path($path) {
    $h = (Get-PsProvider 'FileSystem').home; if(!$h.endswith('\')) { $h += '\' }
    if ($h -eq '\') { 
        return $path 
    }
    return "$path" -replace ([regex]::escape($h)), "~\"
}

# operations
function dl($url, $dest) {
    (New-Object System.Net.WebClient).DownloadFile($url, $dest)
}

function unzip($zipfile, $dest) {
    if (!(Test-Path $zipfile)) { 
        abort "can't find $zipfile to unzip" 
    }
    
    try {
        Add-Type -AssemblyName System.IO.Compression.FileSystem -ea stop 
    } catch {
        # for .net earlier than 4.5
        unzip_old($zipfile, $dest) 
        return 
    } 
    
    try {
        [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $dest)
    } catch {
        abort "Unzip failed: $_"
    }
}

function unzip_old($zipfile, $dest) {
    # fallback for .net earlier than 4.5
    $shell = (new-object -com shell.application -strict)
    $zipfiles = $shell.namespace("$zipfile").items()
    $dest = ensure $dest
    $shell.namespace("$dest").copyHere($zipfiles, 4) # 4 = don't show progress dialog
}

function shim($path, $name, $arg) {
    if (!(test-path $path)) { 
        abort "Can't shim '$(fname $path)': couldn't find '$path'." 
    }
    
    $abs_shimdir = ensure($shimdir)
    if (!$name) { 
        $name = strip_ext(fname $path) 
    }

    $shim = "$abs_shimdir\$($name.tolower())"

    # convert to relative path
    Push-Location $abs_shimdir
    $relative_path = Resolve-Path -relative $path
    Pop-Location
    $resolved_path = Resolve-Path $path

    # if $path points to another drive resolve-path prepends .\ which could break shims
    if ($relative_path -match "^(.\\[\w]:).*$") {
        write-output "`$path = `"$path`"" | out-file "$shim.ps1" -encoding utf8
    } else {
        # Setting PSScriptRoot in Shim if it is not defined, so the shim doesn't break in PowerShell 2.0
        Write-Output "if (!(Test-Path Variable:PSScriptRoot)) { `$PSScriptRoot = Split-Path `$MyInvocation.MyCommand.Path -Parent }" | Out-File "$shim.ps1" -Encoding utf8
        write-output "`$path = join-path `"`$psscriptroot`" `"$relative_path`"" | out-file "$shim.ps1" -Encoding utf8 -Append
    }

    if ($path -match '\.jar$') {
        "if(`$myinvocation.expectingInput) { `$input | & java -jar `$path $arg @args } else { & java -jar `$path $arg @args }" | out-file "$shim.ps1" -encoding utf8 -append
    } else {
        "if(`$myinvocation.expectingInput) { `$input | & `$path $arg @args } else { & `$path $arg @args }" | out-file "$shim.ps1" -encoding utf8 -append
    }

    if ($path -match '\.exe$') {
        # for programs with no awareness of any shell
        Copy-Item "$(versiondir 'scoop' 'current')\supporting\shimexe\bin\shim.exe" "$shim.exe" -force
        write-output "path = $resolved_path" | out-file "$shim.shim" -encoding utf8
        if($arg) {
            write-output "args = $arg" | out-file "$shim.shim" -encoding utf8 -append
        }
    } elseif ($path -match '\.((bat)|(cmd))$') {
        # shim .bat, .cmd so they can be used by programs with no awareness of PSH
        "@`"$resolved_path`" $arg %*" | out-file "$shim.cmd" -encoding ascii

        "#!/bin/sh`nMSYS2_ARG_CONV_EXCL=/C cmd.exe /C `"$resolved_path`" $arg `"$@`"" | out-file $shim -encoding ascii
    } elseif ($path -match '\.ps1$') {
        # make ps1 accessible from cmd.exe
        "@echo off
setlocal enabledelayedexpansion
set args=%*
:: replace problem characters in arguments
set args=%args:`"='%
set args=%args:(=``(%
set args=%args:)=``)%
set invalid=`"='
if !args! == !invalid! ( set args= )
powershell -noprofile -ex unrestricted `"& '$resolved_path' $arg %args%;exit `$lastexitcode`"" | out-file "$shim.cmd" -encoding ascii

        "#!/bin/sh`npowershell.exe -noprofile -ex unrestricted `"$resolved_path`" $arg `"$@`"" | out-file $shim -encoding ascii
    } elseif ($path -match '\.jar$') {
        "@java -jar `"$resolved_path`" $arg %*" | out-file "$shim.cmd" -encoding ascii
        "#!/bin/sh`njava -jar `"$resolved_path`" $arg `"$@`"" | out-file $shim -encoding ascii
    }
}

function ensure_path() {
    $dir = ensure $shimdir
    $path = env 'PATH'
    $dir = fullpath $dir
    if ($path -notmatch [regex]::escape($dir)) {
        write-output "Adding $(friendly_path $dir) to your path."

        env 'PATH' "$dir;$path" # for future sessions...
        $env:PATH = "$dir;$env:PATH" # for this session
    }
}

function remove_path() {
    $dir = fullpath $shimdir

    # future sessions
    $was_in_path, $newpath = strip_path(env 'PATH') $dir
    if ($was_in_path) {
        write-output "Removing $(friendly_path $dir) from your path."
        env 'PATH' $newpath
    }

    # current session
    $was_in_path, $newpath = strip_path $env:PATH $dir
    if ($was_in_path) { 
        $env:PATH = $newpath 
    }
}

function strip_path($orig_path, $dir) {
    if($null -eq $orig_path) { 
        $orig_path = '' 
    }
    $stripped = [string]::join(';', @( $orig_path.split(';') | Where-Object { $_ -and $_ -ne $dir } ))
    return ($stripped -ne $orig_path), $stripped
}

function env($name, $val='__get') {
    if ($val -eq '__get') { 
        [environment]::getEnvironmentVariable($name, 'User') 
    } else { 
        [environment]::setEnvironmentVariable($name, $val, 'User') 
    }
}

function rm_dir($dir) {
    try {
        Remove-Item -r -force $dir -ea stop
    } catch {
        abort "Couldn't remove $(friendly_path $dir): $_"
    }
}